

// Reset the contents of every sequence, including SIZE values
void resetAll() {
  memset(CMD, 0, 48);
  memset(POS, 0, 96);
  memset(STATS, 0, 48);
}

// Reset timing of all seqs and the global-cue, and send a SONG-POSITION POINTER if CLOCK SOURCE is internal
void resetTiming() {
  
  // Reset the global cue-point so that it will wrap around to 0 the sequencer's next tick
  CURTICK = (word(RANGE) * word(TIMESIG3)) - 1;
  
  // Reset the variable that tracks how many external ticks are in each internal tick
  INPULSE = CLOCKSOURCE ? (CLOCKSOURCE - 1) : 0;
  
  memset(POS, 0, 96); // Reset each seq's internal tick-position
  
  if (!CLOCKSOURCE) { // If the CLOCK SOURCE is currently in "internal" mode (and not "MIDI-IN" mode)...
    sendClockReset(); // Send a MIDI-CLOCK reset command to MIDI-OUT
  }
  
}

// Send a MIDI-CLOCK reset command to MIDI-OUT
void sendClockReset() {

  byte spos[6] = { // Create a series of commands to stop, adjust, and restart any devices downstream:
    252, // STOP
    242, 0, 0, // SONG-POSITION POINTER: beginning of every sequence
    250, // START
    0 // (Empty array entry)
  };
  Serial.write(spos, 5); // Send the series of commands to the MIDI-OUT circuit
  
  memset(BLINK, 1, 2); // Set a full-screen BLINK for 1 tick...
  TO_UPDATE |= 4; // ... and flag the 3rd LED-row to be updated.
  
}

// Apply the given binary slice-command-value to the given sequence's play-position
void applySlice(byte seq, byte slice, byte align) {

  word sz = word((STATS[seq] & 63) + 1) * TIMESIG2; // Get the size of each slice, in ticks
  
  // Get the number of ticks by which to offset the sequence's position.
  // For fresh sequences, this will be anchored to CURTICK, which is itself wrapped around the TIMESIG3 "global slice" size.
  // For already-playing sequences, this will be anchored to the seq's own contents.
  word fill = align ? (CURTICK % TIMESIG3) : POS[seq];
  
  // Set the seq's position to correspond to the given slice, while preserving its current tick-offset in terms of the current slice.
  POS[seq] = ((sz * ((slice < TIMESIG1) ? slice : TIMESIG1)) + (fill % sz)) % seqLen(seq);

}

// Read a tick from the currently-iterating sequence
void readTick() {
  
  // Navigate to the note's absolute position, and compensate for the fact that each tick contains 10 bytes.
  file.seekSet((FILE_BODY_START + (POS[ITER_SEQ] * 10)) + (BYTES_PER_SEQ * ITER_SEQ));
  
  file.read(BUFREAD, 10); // Read the data of the tick's notes
  
}

// Parse a single note (or other command) in the read-buffer, and send it to MIDI-OUT and to the SUSTAIN buffer as appropriate
void parseNote(byte point, byte dglyph) {
  
  if (EDITMODE) { // If EDIT MODE is active...
    
    if (
      (RECORDSEQ == ITER_SEQ) // If the currently-iterating seq is the one being edited...
      && (!RECORDNOTES) // And RECORDNOTES isn't active...
      && BUFREAD[point] // And this note-slot contains a command...
    ) {
      
      byte cmdtrk = point >> 2; // Get which TRACK the command is on (5 >> 2 = 1)
      
      if (
        (cmdtrk == TRACK) // If this note is on the currently-active TRACK...
        && (!dglyph) // And no GLYPH is already stored from a user REPEAT command...
      ) {
        GLYPHVIS = TIMESIG2; // Start a glyph-decay that's the length of 1 beat
        memcpy(GLYPH, BUFREAD + point, 5); // Copy this note-slot's command into the GLYPH array
        TO_UPDATE |= 248; // Flag the bottom 5 LED-rows for updating
      }
      
      BLINK[cmdtrk] = 2; // Start a 1-tick-long BLINK, linked to the command's TRACK
      TO_UPDATE |= 4; // Flag the 3rd LED-row for updating
      
    }
    
  // Else, if EDIT MODE isn't active, and the note's CHANCE value does not exceed a random value...
  } else if (BUFREAD[point + 4] < RANDS[ITER_SEQ & 3]) {
    
    return; // Exit the function, since commands below the chance-threshold shouldn't be parsed in PLAY MODE
    
  }
  
  byte cmd = BUFREAD[point] & 240; // Get the MIDI-command-type of the incoming command
  
  if (cmd <= 112) { // If the upper command-bits are a sequencer-specific special-command...
    
    if (!EDITMODE) { // If we are in PLAY MODE...
      
      // Play the sequencer-specific special-command that is currently in the BUFREAD-array
      playCustomCommand(BUFREAD[point], BUFREAD[point + 1], BUFREAD[point + 2], BUFREAD[point + 3]);
      
    }
    
    return; // Exit the function, since no SUSTAIN or MIDI-OUT is needed for special-commands
    
  } else if (cmd != 144) { // Else, if this is neither a sequencer-specific special-command nor a NOTE-ON...
    
    // Send the MIDI-command, with the correct number of bytes for its command-type
    Serial.write(BUFREAD + point, 2 | ((BUFREAD[point] % 224) <= 191));
    
    return; // Exit the function and forego any sustain mechanisms
    
  } else { // Else, if this is a NOTE-ON command...
    
    byte ch = BUFREAD[point] & 15; // Get the note's MIDI-CHANNEL, so we can check for any channel-targeted effects
    
    if (
      (SCHAN == ch) // If this command is on the SHIFT-channel...
      && SCOUNT // And SCOUNT still contains a SHIFT-countdown...
    ) {
      BUFREAD[point + 1] += SHIFTAMT; // Add the SHIFT-amount to the pitch-value, allowing for underflow/overflow
      BUFREAD[point + 1] &= 127; // Mask the pitch-value to the valid note-pitch range (0-127)
      SCOUNT--; // Reduce the SHIFT-countdown by 1
    }
    
    if (HCHAN[0] == ch) { // If HOCKET is active (where HCHAN[0]=255 means "off")...
      BUFREAD[point] = 144 | HCHAN[HTOGGLE]; // Put the current HOCKET-channel into the command-byte
      HCOUNT--; // Reduce the HOCKET-countdown by 1
      if (!HCOUNT) { // If this channel's HOCKET-countdown has finished...
        HTOGGLE ^= 1; // Toggle to the other HOCKET-channel
        HCOUNT = HLIMIT[HTOGGLE]; // Set the HOCKET-countdown to the new HOCKET-channel's number of notes
      }
    }
    
    Serial.write(BUFREAD + point, 3); // Send the NOTE-ON
    
    clipBottomSustain(); // If the bottommost SUSTAIN-slot is full, send its NOTE-OFF prematurely
    
    memmove(SUST + 3, SUST, SUST_COUNT); // Move all sustains one space downward
    SUST[0] = BUFREAD[point] & B10001111; // Turn the NOTE-ON command into a NOTE-OFF command for the same channel, and store it
    SUST[1] = BUFREAD[point + 1]; // Store the command's pitch-byte, without modifying it
    SUST[2] = BUFREAD[point + 3]; // Store the command's duration-byte, since velocity-bytes don't need to be stored for sustain-tracking
    SUST_COUNT += 3; // Increase the number of active sustains by 1 sustain (3 bytes)
    
  }
  
}

// Process a REPEAT, REPEAT-RECORD, or REPEAT-ERASE command
void processRepeat(byte ctrl, byte &dglyph) {
  
  if (isInsertionPoint()) { // If this is the current QUANTIZE-QRESET-OFFSET insertion-point...
    
    if (ctrl == BINARY_ERASE) { // If ERASE-NOTES is held...
      
      if (RECORDNOTES) { // If RECORDNOTES is armed...
        
        // Erase any notes within the RECORDSEQ's current QUANTIZE-QRESET-OFFSET-tick
        eraseTick(applyOffset(1, applyQuantize(POS[RECORDSEQ])));
        
        BLINK[TRACK] = 2; // Start a 1-tick-long BLINK on the active TRACK
        TO_UPDATE |= 4; // Flag the 3rd LED-row for updating
        
      }
      
    } else if (ARPACTIVE) { // Else, if ERASE-NOTES isn't held, and a held-REPEAT-command is currently active...
      
      arpAdvance(BUTTONS >> 6); // Advance the arpeggiator-position
      
      byte newpitch = modPitch(ARPPOS); // Apply OCTAVE to the current arpeggiator-pitch, and then keep that pitch in-bounds
      
      HELDBUTTON = 128 | newpitch; // Set the held-button to signify both "currently held" (|128) and the current REPEAT-note
      HELDPOS = applyOffset(1, applyQuantize(POS[RECORDSEQ])); // Track the position in RECORDSEQ where this note started, with offset/quantize
      
      BUFWRITE[0] = CHAN; // First held byte: Command-type and MIDI-channel
      BUFWRITE[1] = newpitch; // Second held byte: Pitch (or corresponding data-payload-byte)
      BUFWRITE[2] = modVelo(VELO); // Third held byte: Velocity (of corresponding data-payload-byte)
      BUFWRITE[3] = DURATION; // Fourth held byte: Default DURATION value
      BUFWRITE[4] = CHANCE; // Hold the current user-entered CHANCE value
      
      validateHeldBytes(); // Ensure that the command's payload-bytes are in the correct format for their command-type
      
      // Flag that a GLYPH is present, and should not be overridden by other notes on this tick in this TRACK.
      // (Note: The GLYPH itself will by set by playNote(), which is called by the following call to playAutoNote().)
      dglyph = 1;
      
      playAutoNote(); // Play an AUTO-DURATION note
      concludeHeldNote(1, 0); // Conclude held note, recording it but not sending any NOTE-OFF
      
    }
    
  } else { // Else, if this isn't the current insertion point...
    
    if (RECORDNOTES && (ctrl == BINARY_ERASEINVERT)) { // If RECORDNOTES is armed, and ERASE-INVERSE-NOTES is held...
      
      eraseTick(POS[RECORDSEQ]); // Erase any notes that do not occupy QUANTIZE-QRESET-OFFSET ticks
      
      BLINK[TRACK] = 2; // Start a 1-tick-long BLINK on the active TRACK
      TO_UPDATE |= 4; // Flag the 3rd LED-row for updating
      
    }
    
  }
  
}

// Get the currently-iterating seq's contents for this tick, and act on them if applicable.
void getTickNotes(byte ctrl) {

  byte didglyph = 0; // This tracks whether this tick has had a user-generated REPEAT-GLYPH.

  if (
    EDITMODE // If EDIT MODE is active...
    && (RECORDSEQ == ITER_SEQ) // And this is the current RECORDSEQ...
    && REPEAT // And REPEAT is toggled...
    && (BUTTONS & 4294967232UL) // And one of the 24 note-buttons is being held...
    && (ctrl != BINARY_MANUAL) // And MANUAL DURATION isn't being held...
  ) {
    processRepeat(ctrl, didglyph); // Process any REPEAT or REPEAT-ERASE commands that might be occurring
  }
  
  readTick(); // Read the tick from within the currently-iterating seq
  
  if (BUFREAD[0]) { // If the tick's first track contains a command for this tick...
    parseNote(0, didglyph); // Process the first command in the tick, starting at byte 0 (0-indexed)
  }
  if (BUFREAD[5]) { // If the tick's second track contains a command for this tick...
    parseNote(5, didglyph); // Process the second command in the tick, starting at byte 5 (0-indexed)
  }

}

// Parse all CUED-ONs that have been saved from the most-recent parseCues()
void parseCuedOns() {
  
  for (byte i = 0; i != CUED_ONS_COUNT; i++) { // For every seq with a lingering CUED-ON command...
  
    byte alreadyon = STATS[CUED_ONS[i]] & 128; // Check if the seq is already playing
    byte isroom = ACTIVE_SEQS < MAX_ACTIVE_SEQS; // Check if there is enough room for another new active-sequence
    
    if (alreadyon) { // If the seq is already playing...
      
      // Reset the seq-position, so the following call to applySlice() doesn't keep any remainder-ticks from seqs' old positions
      POS[CUED_ONS[i]] = 0;
      
    } else if (isroom) { // Else, if the seq is not already playing, and there is room for another active-seq...
      
      STATS[CUED_ONS[i]] |= 128; // Enable the seq's playing-bit
      ACTIVE_SEQS++; // Increase the "number of currently-active seqs" value
      
    }
    
    if (alreadyon || isroom) { // If the seq is already on, or there was room for another active-seq...
      
      // Get the actual value of the cued slice-command, and send it to the seq-slicing function for this cued-seq
      // Also, fill the seq's new slice with the number of ticks within the current GLOBAL CUE slice.
      applySlice(CUED_ONS[i], (CMD[CUED_ONS[i]] & B00011100) >> 2, 1);
      
    }
    
    CMD[CUED_ONS[i]] = 0; // Clear the sequence's CUED-COMMANDS byte
    
  }
  
  memset(CUED_ONS, 0, MAX_ACTIVE_SEQS); // Unset the CUED_ONS array
  CUED_ONS_COUNT = 0; // Reset the CUED_ONS counting-var
  
}

// Parse all sequences' cued-commands, saving CUED-ONs for later
void parseCues() {
  
  for (byte s = 0; s < 48; s++) { // For every sequence in the song...
    
    if (CMD[s]) { // If the seq has any cued commands...
      
      if (!(byte(CURTICK % TIMESIG2) & 254)) { // If this is the first or second tick in a given global time-signature-divisor...
        TO_UPDATE |= 4 << ((s % 24) >> 2); // Flag the sequence's corresponding LED-row for an update
      }
      
      if (CURTICK == (word((CMD[s] & B11100000) >> 5) * TIMESIG3)) { // If the seq's stored cue-point matches the current global tick...
        
        if (CMD[s] & 2) { // If this is a cued-ON command, save it until after all the cued-OFFs have been processed
          if (CUED_ONS_COUNT == MAX_ACTIVE_SEQS) { continue; } // If the last CUED-ON slot is already filled, ignore this command
          CUED_ONS[CUED_ONS_COUNT] = s; // Save the seq's key into the highest unfilled CUED_ONS slot
          CUED_ONS_COUNT++; // Increase the var that counts how many cued-ONs are stored
          continue; // Move on to the next seq, since cued-ONs must be processed after all parseCues() activity has finished
        }
        
        // At this point in the function, only OFF commands are being parsed. So...
        
        if (STATS[s] & 128) { // If the seq is currently playing...
          STATS[s] &= 127; // Unset the seq's "currently playing" bit
          ACTIVE_SEQS--; // Reduce the "total active seqs" count by 1
        }
        
        CMD[s] = 0; // Clear the sequence's CUED-COMMANDS byte
        
      }
      
    }
    
  }
  
}

// Iterate all seqs, processing their CUEs, and processing any commands within their current ticks
void iterateSeqs(byte advance) {
  
  byte ctrl = BUTTONS & B00111111; // Get the control-row buttons' activity
  
  parseCues(); // Parse all sequences' cued-commands, saving CUED-ONs for later
  parseCuedOns(); // Parse all CUED-ONs that have been saved from the parseCues() call
  
  // Note: ITER_SEQ is a global variable, so it doesn't have to be initialized in the for-loop declaration.
  for (ITER_SEQ = 0; ITER_SEQ < 48; ITER_SEQ++) { // For every sequence in the song...
    
    if (STATS[ITER_SEQ] & 128) { // If the seq is currently playing...
      
      getTickNotes(ctrl); // Get the given seq's contents for this tick, and send any MIDI commands to the MIDI-OUT port.
      
      // If this func was called with "advance", increase the seq's tick-position by that number, wrapping it around its top limit
      POS[ITER_SEQ] = (POS[ITER_SEQ] + advance) % seqLen(ITER_SEQ);
      
    }
    
  }
  
}

// Advance global tick, iterate through all currently-active sequences, and advance the global SUSTAIN-system
void iterateAll() {
  iterateSeqs(1); // Iterate all seqs, processing their CUEs, and processing any commands within their current ticks
  processSustains(); // Process one tick's worth of duration for all sustained notes
}


