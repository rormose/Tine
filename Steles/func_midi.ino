

// MIDI PANIC: Send "ALL NOTES OFF" CC-commands on every MIDI channel.
void midiPanic() {
  byte note[4] = {176, 123, 0, 0}; // Create a generic "ALL NOTES OFF" CC command
  for (; note[0] <= 191; note[0]++) { // For every MIDI channel...
    Serial.write(note, 3); // Send the ALL-NOTES-OFF CC command to MIDI-OUT immediately
  }
}

// Parse a given incoming MIDI command from the MIDI-IN port
void parseMidiCommand() {

  if (
    ((INBYTES[0] & 240) == 144) // If the incoming command is a NOTE-ON...
    && (!INBYTES[2]) // And its VELOCITY-byte is 0...
  ) { // That means this is a NOTE-OFF expressed by a second type of running-status-byte (2nd data-byte, instead of header-byte). So...
    INBYTES[0] = 128 | (INBYTES[0] & 15); // Convert the incoming command into a NOTE-OFF, preserving its CHANNEL information
  }
  
  if (
    (INBYTES[0] == 242) // If this is a SONG-POSITION POINTER...
    && (CLOCKSOURCE) // And CLOCK SOURCE is "from MIDI-IN"...
    && (!(INBYTES[1] | INBYTES[2])) // And this command is specifically for SONG POSITION 0...
  ) {
  
    resetTiming(); // Reset timing of all seqs and the global-cue
    Serial.write(INBYTES, 3); // Send the 3-byte command onward to MIDI-OUT
    
  } else if (EDITMODE) { // Else, if this isn't a specific SONG-POSITION POINTER, and EDIT MODE is active...
    
    if (byte(BUTTONS & B00111111) == BINARY_MANUAL) { // If MANUAL-DURATION is currently active, treat this like a MANUAL-DUR note:
      
      parseManualInput(1, 127, INBYTES[0], INBYTES[1], INBYTES[2]); // Parse a MANUAL-DURATION input based on this incoming-command
      
    } else { // Else, if MANUAL-DURATION is inactive, treat this like a default-DURATION note:
      
      parseAutoInput(1, 127, INBYTES[0], INBYTES[1], INBYTES[2]); // Parse an AUTO-DURATION input based on this incoming-command
      
    }
    
  } else { // Else, if this isn't a specific SONG-POSITION POINTER, and PLAY MODE is active...
    
    Serial.write(INBYTES, 2 | ((INBYTES[0] % 224) <= 191)); // Send the held-command to MIDI-OUT, with the correct number of bytes
    
  }
  
  // NOTE: this code is for testing the incoming MIDI-commands that have been fully processed!
  // Keep it commented out when not in use!!!!
  /*
  for (byte i = 0; i <= 2; i++) {
    sendMaxCmd(5 + i, INBYTES[i]);
  }
  */
  
}

// Parse all incoming raw MIDI bytes
void parseRawMidi() {
  
  while (Serial.available()) { // While new MIDI bytes are available to read from the MIDI-IN port...
    
    byte b = Serial.read(); // Get the frontmost incoming byte
    
    if (SYSIGNORE) { // If this is an ignoreable SYSEX command...
      if (b == 247) { // If this was an END SYSEX byte, clear SYSIGNORE and stop ignoring new bytes
        SYSIGNORE = 0;
      }
      continue; // Regardless, move on to the next byte in the MIDI-IN queue, if there is one
    }
    
    if (INCOUNT) { // If a command's first byte has already been received...
      
      if (b & 128) { continue; } // If this is an interleaved STATUS BYTE, ignore it
      
      INBYTES[INCOUNT] = b; // Store the frontmost incoming byte
      INCOUNT++; // Increase the count of stored incoming bytes
      
      if (INCOUNT == INTARGET) { // If all the command's bytes have been received...
        parseMidiCommand(); // Parse the command
        INCOUNT = 0; // Reset the count of stored incoming bytes
      }
      
      continue; // Move on to the next byte in the MIDI-IN queue, if there is one
      
    }
    
    // At this point, this is either a single-byte command, or a multi-byte command's first byte. So...
    
    byte cmd = b & B11110000; // Get the command-type of any given non-SYSEX command
    
    if (
      (b == 244) // If this is a MISC SYSEX command...
      || (b == 240) // Or a START SYSEX MESSAGE command...
    ) { // Anticipate an incoming SYSEX message
      
      SYSIGNORE = 1;
      
    } else if (
      ((cmd <= 176) && (cmd >= 128)) // Else, if this is CONTROL CHANGE, POLY KEY PRESSURE, NOTE-ON, or NOTE-OFF (all 3-byte)...
      || (cmd == 224) // Or a PITCHBEND command (3-byte)...
      || (cmd == 242) // Or a SONG-POSITION POINTER command (3-byte)...
    ) {
      
      INBYTES[0] = b; // Save the incoming byte
      INCOUNT = 1; // "This message's first byte has been received"
      INTARGET = 3; // "This message will be 3 bytes long"
      
    } else if (
      (b == 243) // Else, if this is a SONG SELECT command...
      || (b == 241) // Or a MIDI TIME CODE QUARTER FRAME command...
      || (cmd == 208) // Or a CHAN PRESSURE (AFTERTOUCH) command...
      || (cmd == 192) // Or a PROGRAM CHANGE command...
    ) { // Anticipate an incoming 2-byte message
      
      INBYTES[0] = b; // Save the incoming byte
      INCOUNT = 1; // "This message's first byte has been received"
      INTARGET = 2; // "This message will be 2 bytes long"
      
    } else if (b >= 244) { // Else, if this is any single-byte command...
      
      if (CLOCKSOURCE) { // If MIDI CLOCK SOURCE is set to "from MIDI-IN"...
        
        Serial.write(b); // Send the single-byte message onward immediately
        
        if (b == 248) { // If this is a MIDI CLOCK tick...
          
          INPULSE = (INPULSE + 1) % CLOCKSOURCE; // Advance the internal PPQN-tracking var by 1, and wrap it by the CLOCKSOURCE val
          
          if (!INPULSE) { // If this is the first pulse of a given tick...
            advanceTick(); // Advance the internal tempo-tick, and all its associated sequencing mechanisms
          }
          
        } else if (b == 250) { // Else, if this is a START command...
          
          resetTiming(); // Reset timing of all seqs and the global-cue
          
        }
        
      }
      
    } else { // Else, this command has "Running Status Byte", where only the data-bytes are sent, and the command-byte is implied. So...
      
      if (!INBYTES[0]) { continue; } // Double-check that this is indeed a Running Status Byte, and not orphaned data on startup
      
      INBYTES[1] = b; // Put the byte into the first payload-byte of the previous command
      
      byte oldcmd = INBYTES[0] & B11110000; // Get the previous command's command-type, stripped of all CHANNEL information
      
      if ((oldcmd == 208) || (oldcmd == 192)) { // If this was a 2-byte command...
        parseMidiCommand(); // Parse the command immediately
        INCOUNT = 0; // Reset the count of stored incoming bytes
        continue; // Move on to the next byte in the MIDI-IN queue, if there is one
      }
      
      // Otherwise, the expected command is 3 bytes in size. So...
      
      if (oldcmd == 128) { // If the previous command was a NOTE-OFF...
        INBYTES[0] |= 16; // Start building a NOTE-ON for the same CHANNEL (turn "128" into "144", leaving CHANNEL bits untouched)
      }
      INCOUNT = 2; // "This message's first 2 bytes have been received"
      INTARGET = 3; // "This message will have 3 bytes"
      
    }
    
  }
  
}
