

// Advance the arpeggiator's position, based on the current ARPMODE
void arpAdvance(unsigned long nbuts) {
  
  if (REPEAT < 3) { // If the current REPEAT MODE is either "up" or "down"...
    
    byte dir = REPEAT == 2; // Convert current arpeggiation-direction to boolean (2=down=1, 1=up=0)
    byte high = ARPPOS; // Highest found-button (holds ARPPOS by default, for when no offsets are found)
    byte low = ARPPOS; // Lowest found-button (holds ARPPOS by default, for when no offsets are found)
    byte dist = 255; // Distance to closest found-button (255 by default, larger than any button-distance could possibly be)
    byte closest = 0; // Closest found-button's value (0 by default, and will either be filled or remain unused)
    
    for (byte i = 0; i < 24; i++) { // For every raw-note-button slot...
      if (nbuts & (1UL << i)) { // If this note-button is held...
        
        // Get the offset-value that corresponds to the given held-button
        byte o = pgm_read_byte_near((GRIDS + (GRIDCONFIG * 24)) + i);
        
        if (o == ARPPOS) { continue; } // If this corresponds to the current ARPPOS-offset, ignore it
        
        high = max(high, o); // If this is the highest found-offset, save it
        low = min(low, o); // If this is the lowest found-offset, save it
        
        byte newdist = abs(char(ARPPOS) - o); // Get this offset's distance from the ARPPOS value
        
        if (
          (newdist < dist) // If the new distance is smaller than the old distance...
          && ( // And...
            (dir && (o < ARPPOS)) // Either, [ARPMODE is "down", and the offset is lower than ARPPOS]...
            || ((!dir) && (o > ARPPOS)) // Or, [ARPMODE is "up", and the offset is higher than ARPPOS]...
          )
        ) {
          closest = o; // Update "closest" to hold the new closest offset
          dist = newdist; // Update "dist" to hold the closest offset's distance
        }
        
      }
    }
    
    if (
      (!ARPLATCH) // If this is the first ARP-offset to be played in the current keystroke-cluster...
      || (dist == 255) // Or dist is still 255, and no nearby offset-candidate has been found...
    ) {
      ARPPOS = dir ? high : low; // Set the new ARPPOS to the highest offset in "down"-mode, or the lowest offset in "up"-mode
    } else { // Else, if a nearby-offset has been found...
      ARPPOS = closest; // Put it directly into ARPPOS
    }
    
  } else { // Else, if the current ARP MODE is "random"...
    
    byte found[25]; // Temp array that will hold all held-button locations
    byte f = 0; // Counts the number of button-locations that will be found and placed into the temp array
    
    for (byte i = 0; i < 24; i++) { // For every button-location...
      if (nbuts & (1UL << i)) { // If this button is currently pressed...
        found[f] = i; // Put the button-location into the temp-array
        f++; // Increase the "number of buttons found" counter
      }
    }
    
    // Out of all the currently-pressed buttons, select one at random, and get its offset-value via the current GRIDCONFIG
    ARPPOS = pgm_read_byte_near(GRIDS + (GRIDCONFIG * 24) + found[RANDS[0] % f]);
    
  }
  
  ARPLATCH = 1; // Set a flag that signifies: "notes have played during the current keypress-cluster"
  
}

// Parse a new keypress in the arpeggiation-system
void arpPress() {
  
  // Note: This function is only called from functions that already check whether REPEAT is active,
  // so we don't need to check for that again here.
  
  if (ARPACTIVE) { return; } // If arpeggiation is already active, exit the function
  
  // Get the note-button bits only, with the control-button bits shifted out of the value
  unsigned long nbuts = BUTTONS >> 6;
  
  if (REPEAT < 3) { // If the current REPEAT MODE is either 1 (up) or 2 (down)...
    
    for (byte i = 0; i < 24; i++) { // For every raw button-position...
      if (nbuts & (1UL << i)) { // If this note-button is held...
        
        // Get an ARPPOS that corresponds to the held-button in question
        ARPPOS = pgm_read_byte_near(GRIDS + (GRIDCONFIG * 24) + i);
        
        ARPACTIVE = 1; // Set arpeggiation to ACTIVE
        
        break; // An initial ARPPOS has been found, so don't search for any more of them
        
      }
    }
    
  } else { // Else, if the current ARP MODE is "random"...
    
    arpAdvance(nbuts); // Advance the arpeggiator's position in a specifically random way
    
    ARPACTIVE = 1; // Set arpeggiation to ACTIVE
    
  }
  
}

// Parse a new key-release in the arpeggiation-system (and check whether it was the last held arp-button)
void arpRelease() {
  if (
    ARPACTIVE // If notes are currently being arpeggiated...
    && (!(BUTTONS >> 6)) // And no other note-buttons are being held...
  ) {
    arpClear(); // Clear the arpeggiation-system's contents
  }
}

// Clear the arpeggiation-system's contents
void arpClear() {
  ARPACTIVE = 0; // Clear the "arpeggiation-system is active" flag
  ARPPOS = 0; // Clear the arpeggiator's offset-pitch, and its activity-flag (128)
  ARPLATCH = 0; // Clear the "notes have played during the current keystroke-cluster" flag
}


