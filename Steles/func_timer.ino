

// Advance the internal tempo-tick, and all its associated sequencing mechanisms
void advanceTick() {
  
  CURTICK = (CURTICK + 1) % (word(RANGE) * word(TIMESIG3)); // Advance the global tick, wrapping it at the current TIMESIG*RANGE point
  
  if (HELDBUTTON & 128) { // If a command is currently being held...
    if (TO_WRITE) { // If a special-command has been set aside for later writing into the savefile...
      concludeHeldNote(1, 1); // Record the stored "about-to-be-recorded" command into the stored savefile-position, if applicable
      TO_WRITE = 0; // Empty out the "a command needs to be written" flag
    } else { // Else, is a special-command wasn't set aside...
      if (BUFWRITE[3] == 255) { // If the note's duration-counter has reached its maximum possible length...
        concludeHeldNote(1, 1); // Conclude held note, recording it and sending a NOTE-OFF
      } else { // Else, if the note's tick-counter hasn't reached its maximum possible length...
        BUFWRITE[3]++; // Increase the note's duration-counter by 1
      }
    }
  }
  
  TO_UPDATE |= !(CURTICK % TIMESIG3); // If this is the first tick within the current GLOBAL CUE slice, flag the top LED-row for updating
  
  if (EDITMODE) { // If we're in EDIT MODE...
    if (RECORDNOTES && isInsertionPoint()) { // If RECORD NOTES is armed, and this is the current QUANTIZE-QRESET-OFFSET insertion-point...
      BLINK[TRACK] = 2; // Start a 1-tick-long BLINK on the active TRACK
      TO_UPDATE |= 4; // Flag the 3rd LED-row for updating
    }
  }
  
  // If this is the start of a global timesig-divisor, flag the second LED-row to update the musician-time-counting display
  TO_UPDATE |= (!(CURTICK % TIMESIG2)) << 1;
  
  iterateAll(); // Iterate through a step of each active sequence
  
  if (EDITMODE) { // If we're in EDIT MODE...
    
    TO_UPDATE |= // Add the following composite flags to the LED-update flag:
      (
        (BLINK[0] ? (!(--BLINK[0])) : 0) // If BLINK[0] is active, reduce it by 1. If that sets it to 0, flag an update.
        | (BLINK[1] ? (!(--BLINK[1])) : 0) // ^ ...same for BLINK[1]
      )
      << 2; // If either of these is true, flag the 3rd LED-row for updating.
      
    TO_UPDATE |= GLYPHVIS ? ((!(--GLYPHVIS)) * 248) : 0; // If GLYPHVIS is active, reduce it by 1. If that sets it to 0, flag an update.
    
  } else { // Otherwise, if we're in PLAY MODE...
    
    TO_UPDATE |= LOADHOLD ? ((!(--LOADHOLD)) * 248) : 0; // If LOADHOLD is active, reduce it by 1. If that sets it to 0, flag an update.
    
  }
  
  xorShift(); // Update the global semirandom values
  
}

// Update the internal timer system, and trigger various events
void updateTimer() {
  
  { // Tidy up memory usage for "micr" and "offset"
    
    unsigned long micr = micros(); // Get the current microsecond-timer value
    unsigned long offset; // Will hold the time-offset since the last ABSOLUTETIME point
    
    if (micr < ABSOLUTETIME) { // If the micros-value has wrapped around its finite counting-space to be less than the last absolute-time position...
      offset = (4294967295UL - ABSOLUTETIME) + micr; // Get an offset representing time since last check, wrapped around the unsigned long's limit
    } else { // Else, if the micros-value is greater than or equal to last loop's absolute-time position...
      offset = micr - ABSOLUTETIME; // Get an offset representing time since last check
    }
    KEYELAPSED += offset; // Add the difference between the current time and the previous time to the elapsed-time-for-keypad-checks value
    ELAPSED += offset; // ^ Same, but for the general elapsed-time value
    
    ABSOLUTETIME = micr; // Set the absolute-time to the current time-value
    
  }
  
  if (KEYELAPSED >= SCANRATE) { // If the keypad-check timer has reached a time where the keypad should be checked...
    scanKeypad(); // Scan the keypad for changes in keystroke values
    KEYELAPSED = 0; // Reset the keypad-check timer
  }
  
  while (ELAPSED >= TEMPO) { // While an amount of time has elapsed that is greater than the current TEMPO...
    
    ELAPSED -= TEMPO; // Subtract the current tick-size-in-microseconds from the elapsed-time variable
    
    if (CLOCKSOURCE) { // If CLOCKSOURCE is "external"...
      if (EDITMODE && (byte(BUTTONS & B00111111) == BINARY_MOVEBYTICK)) { // If we're in EDIT MODE, and MOVE BY TICK is being held...
        processSustains(); // Process one tick's worth of duration for all sustained notes
      }
    } else { // Else, if CLOCKSOURCE is "internal"...
      Serial.write(248); // Immediately send a MIDI CLOCK pulse to MIDI-OUT
      advanceTick(); // Advance the tempo-tick, and all its associated sequencing mechanisms
    }
    
  }
  
}


