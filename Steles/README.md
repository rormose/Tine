
# Steles
Steles is the name of the default MIDI-sequencer firmware for the Tine hardware. It is the central focus of the Tine project, and may as well be synonymous with Tine itself. But for historical reasons, and possibly also futureproofing reasons, this sequencing firmware is indeed technically called "Steles".
