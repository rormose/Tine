


// Play a sequencer-specific special-command
void playCustomCommand(byte cmd, byte b1, byte b2, byte b3) {
  
  if (cmd == 16) { // If this is a SNIP command, go through the list of held sustains and halt all of them
    
    while (SUST_COUNT) { // While there are any active SUSTAINs...
      endSustain(0); // End the topmost queued SUSTAIN
    }
    
  } else if (cmd == 32) { // Else, if this is a JUMP command, move the current seq by a specific number of ticks/measures:
    
    word len = seqLen(ITER_SEQ); // Get the length of the currently-parsing seq, in ticks
    
    // If this is "jump by measure": get a number of ticks equal to the given number of measures, bounded by the seq's size.
    // Else, if this is "jump by tick": get the given number of ticks, bounded by the seq's size (in case the seq is tiny).
    word dist = (b2 & 64) ? (word((b3 & 63) % ((STATS[ITER_SEQ] & 63) + 1)) * TIMESIG3) : (b3 % len);
    
    // Apply the jump-distance, either positive or negative wrt "jump forward" or "jump backward", and bound it to the seq's size.
    POS[ITER_SEQ] = (int(POS[ITER_SEQ]) + ((b2 & 32) ? dist : (-int(dist))) + len) % len;
    
  } else if (cmd == 48) { // Else, if this is a REALIGN command, change slice-position and/or realign with the global cue:
    
    word len = seqLen(ITER_SEQ); // Get the length of the currently-parsing seq, in ticks
    
    // Set vars for upcoming manipulation, based on whether this is a "realign forward" or "realign backward" command:
    char oset1 = (b3 ? CURTICK : POS[ITER_SEQ]) % TIMESIG3; // If forward: 1st offset is related to global-cue. Else seq-position.
    char oset2 = (b3 ? POS[ITER_SEQ] : CURTICK) % TIMESIG3; // If forward: 2nd offset is related to seq-position. Else global-cue.
    
    POS[ITER_SEQ] += len; // Add the current-seq's length-in-ticks to its position, to prevent underflows
    POS[ITER_SEQ] += (oset1 - oset2) + ((oset1 < oset2) ? TIMESIG3 : 0); // Realign the seq-position either forward or backward
    POS[ITER_SEQ] %= len; // Bound the seq's modified position within its length-in-ticks, resulting in the desired position
    
  } else if (cmd == 64) { // Else, if this is a TIMESIG command, change all TIMESIG variables immediately
    
    TIMESIG1 = b2; // Get the incoming time-signature dividend and divisor, respectively
    TIMESIG2 = b3; // ^
    
    tsUpdateWrap(); // Update TIMESIG3, then wrap the CURTICK around the number of total measures, to keep it from going out-of-bounds
    
  } else if (cmd <= 95) { // Else, if this is a HOCKET command, initialize the HOCKET system
    
    HCHAN[0] = b3 ? (cmd & 15) : 255; // If this is a HOCKET-off command, set HCHAN[0] to 255, else set HCHAN[0] to the command's chan-value
    HCHAN[1] = b2; // Set the hocket's alternating channel to the velocity-byte, which has already been limited to 0-15.
    HLIMIT[0] = b3 & 15; // First hocket-section limit: LSBx4 of duration-byte
    HLIMIT[1] = b3 >> 4; // Second hocket-section limit: MSBx4 of duration-byte
    HCOUNT = HLIMIT[0]; // Start a HOCKET-countdown with the number of notes in the first channel's hocket-section
    
  } else if (cmd <= 111) { // Else, if this is a SHIFT command, initialize the SHIFT system
    
    SCHAN = (cmd & 15) | ((!(b3 & 127)) << 7); // Set SHIFT's target-channel to the command's chan-value, or to 128|junk if the given duration-byte was 0 (this ends SHIFT immediately)
    SCOUNT = b2; // Start a SHIFT-countdown with the modified velocity-byte value (possible values: 1 to 127)
    SHIFTAMT = char(b3 & 127) * ((b3 & 128) ? -1 : 1); // Duration-byte: Amount to shift the target-channel's pitches by (128=negative)
    
  } else { // Else, if this was a TEMPO command (cmd == 112), change the local TEMPO immediately
    
    // Set TEMPO to a composite of the 2 given byte-values, bounded to TEMPO's limits
    TEMPO = word(b1 | b2, b3);
    TEMPO = (TEMPO < TEMPO_LIMIT_LOW) ? TEMPO_LIMIT_LOW : TEMPO;
    
  }
  
}

// Play an AUTO-DURATION note (note: this function is only called from Edit Mode)
void playAutoNote() {
  
  // If MODIFY and RECORDNOTES are simultaneously active, don't send any command to MIDI-OUT
  if (MODIFY && RECORDNOTES) { return; }
  
  if ((BUFWRITE[0] & 240) == 144) { // If this was a NOTE-ON, update the SUSTAIN buffer to prepare to play the user-pressed note:
    
    clipBottomSustain(); // If the bottommost SUSTAIN is filled, send its NOTE-OFF prematurely
    
    memmove(SUST + 3, SUST, SUST_COUNT); // Move all sustains one space downward
    SUST[0] = BUFWRITE[0] & B10001111; // Fill the topmost sustain-slot with the user-pressed note's corresponding NOTE-OFF command
    SUST[1] = BUFWRITE[1]; // ^
    SUST[2] = BUFWRITE[3]; // ^ (Put DUR in the sustain's third byte, to be replaced with a dummy-velocity-byte after it counts down)
    SUST_COUNT += 3; // Increase the number of active sustains by 1 (i.e. 3 bytes), to accurately reflect the new note
    
  }
  
  // Play the held-NOTE-ON whose corresponding NOTE-OFF was just put into the SUSTAIN buffer,
  // or play any other held-MIDI-command that has no corresponding NOTE-OFF.
  playNote();
  
}

// Send the current held-command, and activate the GUI to reflect it (note: this function is only called from Edit Mode)
void playNote() {
  
  // If MODIFY and RECORDNOTES are simultaneously active, don't send any held-notes to MIDI-OUT
  if (MODIFY && RECORDNOTES) { return; }
  
  if (BUFWRITE[0] >= 144) { // If this is a normal MIDI-command (and not a NOTE-OFF)...
    Serial.write(BUFWRITE, 2 | ((BUFWRITE[0] % 224) <= 191)); // Send the held-command to MIDI-OUT, with the correct number of bytes
  }
  
  GLYPHVIS = TIMESIG2; // Start a glyph-decay that's the length of 1 beat
  memcpy(GLYPH, BUFWRITE, 5); // Copy the held-command from BUFWRITE to GLYPH as well
  BLINK[TRACK] = 2; // Start a 1-tick-long BLINK for the current TRACK
  TO_UPDATE |= 252; // Flag the 3rd LED-row (BLINK), and the bottom 5 LED-rows (GLYPH), for updating
  
}

// Optionally record the currently-held command, optionally end the held-note, and then clear the held-command's "held" status
void concludeHeldNote(byte recordcmd, byte endnote) {
  
  if (HELDBUTTON & 128) { // If there is currently a held-command...
    
    if (recordcmd && RECORDNOTES) { // If note-recording is armed...
      writeCommand(FILE_BODY_START + (BYTES_PER_SEQ * RECORDSEQ) + (HELDPOS * 10) + (TRACK * 5)); // Write the held-command to the current held-data-position
    }
    
    if (
      endnote // If this function-call has an "end the currently-held-note" flag...
      && ((BUFWRITE[0] & 240) == 144) // And the current held-command is a NOTE-ON...
    ) {
      BUFWRITE[0] &= B10001111; // Change the note's command-byte into a NOTE-OFF, preserving its MIDI-CHANNEL number
      Serial.write(BUFWRITE, 3); // Send the NOTE-OFF
    }
    
    HELDBUTTON &= 127; // Clear the "held note" flag (this flag is stored in HELDBUTTON|128)
    
  }
  
}

// Make changes to a held-note to bring it in line with Tine's internal-command spec or the MIDI spec
void validateHeldBytes() {
  
  byte cmd = BUFWRITE[0] & 240; // Get the MIDI-command-type, stripped of CHANNEL information
  
  if (cmd == 16) { // If this is a SNIP command...
    
    BUFWRITE[0] = cmd; // Strip all CHANNEL bits from the held MIDI-command byte
    memset(BUFWRITE + 1, 0, 3); // Set BUFWRITE items 1-3 to "0", keeping item 4 as whatever the CHANCE value was
    
  } else if (cmd == 32) { // Else, if this is a JUMP command...
    
    BUFWRITE[0] = cmd; // Strip all CHANNEL bits from the held MIDI-command byte
    BUFWRITE[1] = 0; // Empty out the pitch-byte
    BUFWRITE[2] &= 96; // Held-velocity becomes: (64 or 0) "jump by measure" or "jump by tick"; (32 or 0) "forward" or "back".
    BUFWRITE[3] |= !BUFWRITE[3]; // Held-duration becomes: Jump-size (1-255).
    
  } else if (cmd == 48) {  // Else, if this is a REALIGN command...
    
    BUFWRITE[0] = cmd; // Strip all CHANNEL bits from the held MIDI-command byte
    memset(BUFWRITE + 1, 0, 2); // Set pitch-byte and velocity-byte to 0
    BUFWRITE[3] = (!BUFWRITE[3]) - 1; // Held-duration becomes: (1-255 -> 255) "forward" or (0) "backward".
    
  } else if (cmd == 64) { // Else, if this is a TIME-SIGNATURE command...
    
    BUFWRITE[0] = cmd; // Strip all CHANNEL bits from the held MIDI-command byte
    BUFWRITE[1] = 0; // Empty out the pitch-byte
    BUFWRITE[2] = (BUFWRITE[2] % 8) + 1; // Held-velocity becomes: a valid timesig dividend (1 to 8)
    BUFWRITE[3] = (BUFWRITE[3] % 6) + 7; // Held-duration becomes: a valid timesig divisor (7 to 12)
    
  } else if (cmd == 80) { // Else, if this is a HOCKET command...
    
    BUFWRITE[1] = 0; // Clear the pitch-byte
    BUFWRITE[2] &= 15; // Mask the velocity-byte to the 0-15 range. This will represent the second HOCKET-channel.
    BUFWRITE[3] *= !((!(BUFWRITE[3] & 240)) | (!(BUFWRITE[3] & 15))); // If both nibbles of the dur-byte are valid HOCKET-lengths, set a HOCKET command, else set a HOCKET-off command
    
  } else if (cmd == 96) { // Else, if this is a SHIFT command...
    
    BUFWRITE[1] = 0; // Clear the pitch-byte
    BUFWRITE[2] += !BUFWRITE[2]; // Make sure the velocity-byte is at least 1. This will represent how many notes to shift before the SHIFT-command expires.
    // BUFWRITE[3] stays the same for this.
    
  } else if (cmd == 112) { // Else, if this is a TEMPO command...
    
    BUFWRITE[0] = cmd; // Strip all CHANNEL bits from the held MIDI-command byte
    BUFWRITE[1] <<= 7; // Get the LSB of the given pitch-byte, and make it into the MSB of the new TEMPO value.
    // BUFWRITE[2] and BUFWRITE[3] stay the same for this.
    
  } else { // Else, if this isn't a sequencer-specific special command...
    
    BUFWRITE[1] &= 127; // Strip the MSB from both payload-bytes, since it should not be present in any regular MIDI command
    BUFWRITE[2] &= 127; // ^
    
  }
  
}


