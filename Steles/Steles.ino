

/*

  Steles is a MIDI sequencer firmware for the "Tine" hardware.
  THIS CODE IS UNDER DEVELOPMENT AND DOESN'T DO ANYTHING!
  Copyright (C) 2016-2024, Zriph Argr (sevenplagues@gmail.com).

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

*/



/////////////////////////////
// Start pragma statements //
/////////////////////////////

// Optimize the compiler for program-speed, at the expense of increased program-size.
#pragma GCC optimize ("-O2")

///////////////////////////
// End pragma statements //
///////////////////////////



/////////////////////////////
// Start define statements //
/////////////////////////////

// These values may need to be changed in the course of programming/debugging,
// but will always stay the same at runtime.

#define TOTAL_SONG_FILES 96 // Total number of song-savefiles

#define FILE_BYTES 2949363UL // Number of bytes in each savefile

// Location of bytes in a given savefile's header-block:
#define FILE_TEMPO_BYTE1 0 // First TEMPO byte
#define FILE_TEMPO_BYTE2 1 // Second TEMPO byte
#define FILE_TIMESIG1_BYTE 2 // TIME-SIGNATURE-DIVIDEND byte
#define FILE_TIMESIG2_BYTE 3 // TIME-SIGNATURE-DIVISOR byte
#define FILE_RANGE_BYTE 10 // CUE-RANGE byte

#define FILE_SQS_START 51 // Start-byte of the seq-size-values block
#define FILE_SQS_END 98 // ^ End-byte

#define FILE_BODY_START 243UL // Start of the file's body-block (UL because large values will be added to this)

#define BYTES_PER_SEQ 61440UL // Bytes within each sequence (UL because large values will be added to this)

#define TEMPO_LIMIT_LOW 16384 // Limits to the range of valid TEMPO values
#define TEMPO_LIMIT_HIGH 65535 // ^

#define UPPER_BITS_LOW 16 // Limits to the range of valid UPPER COMMAND BITS values
#define UPPER_BITS_HIGH 224 // ^

#define PREFS_ITEMS_1 17 // Number of items in the PREFS-file (1-indexed)
#define PREFS_ITEMS_2 18 // ^ (1-indexed + 1, for buffer assembly)

#define DEFAULT_TEMPO 40960 // Default TEMPO (in microseconds-per-tick), for cases where a coherent tempo-value is not available
#define DEFAULT_SEQ_SIZE 7 // Default seq-size, for constructing the savefile (0-indexed: 7 = 8 measures)

#define GRID_TOTAL 5 // Number of GRIDCONFIG grids within the GRIDS[] array (this value is 0-indexed!!!)

#define SCANRATE 10001 // Amount of time between keystroke-scans, in microseconds

#define MAX_ACTIVE_SEQS 8 // Maximum number of simultaneously-active sequences. This exists for lag-prevention reasons

// The following define-statements represent the various control-commands' binary button-values.
  // PLAY MODE:
    // 1-keypress:
#define        BINARY_PAGE B00000001 // DO NOT CHANGE
    // 2-keypress:
      //   BINARY_SHIFTPOS B00000011 // GETS DEFINED BELOW
#define        BINARY_SWAP B00000101 // DO NOT CHANGE
#define     BINARY_ENDSUST B00001001
#define       BINARY_RANGE B00100001 // DO NOT CHANGE
    // 3-keypress:
#define       BINARY_LOAD0 B00110001 // DO NOT CHANGE
    // 4-keypress:
#define       BINARY_LOAD1 B00110011 // DO NOT CHANGE
#define       BINARY_LOAD2 B00110101 // DO NOT CHANGE
#define       BINARY_LOAD3 B00111001 // DO NOT CHANGE
    // 5-keypress: (N/A)
    // 6-keypress:
      // BINARY_MODETOGGLE B00111111 // GETS DEFINED BELOW
  // EDIT MODE:
    // 1-keypress:
#define      BINARY_ARMREC B00000001
#define       BINARY_TRACK B00000010
#define    BINARY_VELOCITY B00000100
#define      BINARY_MODIFY B00001000
#define      BINARY_REPEAT B00010000
#define      BINARY_MANUAL B00100000
    // 2-keypress:
#define    BINARY_SHIFTPOS B00000011 // DO NOT CHANGE
#define       BINARY_TEMPO B00000101 // DO NOT CHANGE
#define  BINARY_GRIDCONFIG B00001001
#define    BINARY_BASENOTE B00010001
#define       BINARY_ERASE B00100001
#define    BINARY_HUMANIZE B00000110
#define     BINARY_SEQSIZE B00001010
#define      BINARY_QRESET B00010010
#define      BINARY_OFFSET B00100010
#define    BINARY_QUANTIZE B00001100
#define   BINARY_UPPERCHAN B00010100
#define      BINARY_ONLOAD B00100100
#define    BINARY_DURATION B00011000
#define        BINARY_CHAN B00101000
#define      BINARY_OCTAVE B00110000
    // 3-keypress:
#define  BINARY_MOVEBYTICK B00000111
#define      BINARY_CHANCE B00111000
#define BINARY_CLOCKSOURCE B00100011
#define  BINARY_TSDIVIDEND B00010101
#define   BINARY_TSDIVISOR B00101010
    // 4-keypress:
#define  BINARY_COPY_PAGE1 B00001111 // DO NOT CHANGE
#define  BINARY_COPY_PAGE2 B00111100 // DO NOT CHANGE
#define BINARY_ERASEINVERT B00110011
#define        BINARY_IREP B00101110
    // 5-keypress:
      // (N/A)
    // 6-keypress:
#define  BINARY_MODETOGGLE B00111111 // DO NOT CHANGE

///////////////////////////
// End define statements //
///////////////////////////



//////////////////////////////
// Start include statements //
//////////////////////////////

// Boot-variable access library
#include <avr/boot.h>

// Program-space (PROGMEM) library
#include <avr/pgmspace.h>

// Serial Peripheral Interface library
#include <SPI.h>

// SD-card data-storage library (requires SPI.h)
#include <SdFat.h>

////////////////////////////
// End include statements //
////////////////////////////



// UI vars
byte BLINK[2] = {0, 0}; // When each of this array's slots is filled, they will count down to 0, illuminating their section of the LEDs
unsigned long BUTTONS = 0; // Keeps track of which of the 30 buttons are currently pressed; each button has an on/off bit
byte GLYPH[5] = {0, 0, 0, 0, 0}; // Holds a TRACK-linked MIDI-command that will be assigned a glyph when the EDIT MODE GUI is processed
byte GLYPHVIS = 0; // Controls whether the current TRACK's activity-glyph is visible or not, and will always be counting down to 0
byte LOADHOLD = 0; // Track how many ticks to hold a just-loaded savefile's file-number onscreen
byte PAGE = 0; // Tracks currently-active page of sequences
const char PREFS_FILENAME[] = "P.DAT"; // Filename for the preferences-file on the SD-card
byte TO_UPDATE = 255; // Tracks which rows of LEDs should be updated at the end of a given tick (initially set to 255, to make sure the entire screen is refreshed)

// Timing vars
unsigned long ABSOLUTETIME = 0; // Absolute time elapsed: wraps around after reaching its limit
unsigned long ELAPSED = 0; // Microseconds elapsed since last tick
unsigned long KEYELAPSED = 0; // Microseconds elapsed since last keystroke-scan

// Mode flag vars
byte CLOCKSOURCE = 0; // Flag for the current MIDI-CLOCK source. 0 = "internal"; 1-255 = "n * 24 = external PPQN"
byte EDITMODE = 0; // Tracks whether EDIT MODE is active
byte RECORDNOTES = 0; // Tracks whether notes are currently being recorded into a sequence
byte RECORDSEQ = 0; // Sequence currently being recorded into
byte REPEAT = 0; // Arpeggiation mode. 0 = none; 1 = up; 2 = down; 3 = random
byte TRACK = 0; // Current track within the active sequence to edit with EDITMODE actions

// Arpeggiation vars
byte ARPPOS = 0; // Holds the arpeggiator's current offset-value, corresponding to a button in the current GRIDCONFIG
byte ARPACTIVE = 0; // Tracks whether arpeggiation is currently being held
byte ARPLATCH = 0; // Tracks whether any arpeggiation-notes have played yet within the current cluster of keystrokes

// EDIT MODE vars
byte BASENOTE = 0; // Current base-note in the playable scale (0 to 11, C to B)
byte CHAN = 144; // MIDI-COMMAND byte (including current CHANNEL) for EDIT MODE notes
byte CHANCE = 255; // Chance that the command that is recorded will be parsed, on any given time it is read
byte DURATION = 1; // Duration value for EDIT MODE notes, in 48th-notes (0 to 255)
byte GRIDCONFIG = 0; // Current note-key grid being used in EDIT MODE
byte HUMANIZE = 0; // Current maximum velocity-humanize value for EDIT MODE notes
byte MODIFY = 0; // Holds flags for which bytes in a recorded note should be overwritten (16=channel, 8=pitch, 4=velo, 2=dur, 1=chance; 0 = record new note)
byte OCTAVE = 4; // Octave-offset value for EDIT MODE notes
char OFFSET = 0; // The number of 48th-notes by which the base QUANTIZE-point gets offset in EDIT MODE (-48 to 48)
byte QRESET = 0; // Tracks how many beats must elapse within RECORDSEQ before the QUANTIZE anchor gets reset (0 = whole sequence)
byte QUANTIZE = 3; // Time-quantize value for EDIT MODE notes (1 to 12)
byte VELO = 127; // Baseline velocity-value for EDIT MODE notes

// Sequencing vars
byte ACTIVE_SEQS = 0; // Counts the number of currently-active seqs. This is hard-limited for lag-prevention reasons
byte CUED_ONS[MAX_ACTIVE_SEQS]; // Temporarily holds the current cued-ON commands while parsing other CUEs
byte CUED_ONS_COUNT = 0; // Counts the current number of held cued-ON commands
word CURTICK = 0; // Current global tick (will always be bounded within the range of 0 to [RANGE * TIMESIG3])
byte HCHAN[2] = {255, 0}; // Active HOCKET-channels (the first of these is the "target channel" for the hocketing system, and is 255 when HOCKET is off)
byte HCOUNT = 0; // Countdown-value for the number of HOCKET-notes that have elapsed
byte HLIMIT[2] = {1, 1}; // Number of notes to count for each channel in the HOCKET system
byte HTOGGLE = 0; // If the HOCKET system is active, this flag signifies whether to send notes from HCHAN[0] or HCHAN[1] when a signal for channel HCHAN[0] is received
byte INPULSE = 0; // Counts the number of external ticks (from MIDI-IN) that have elapsed within the current internal tick
byte ITER_SEQ = 0; // Represents the current seq that is being iterated.
byte SCHAN = 0; // Target-channel for the SHIFT system
byte SCOUNT = 0; // Countdown-value for the number of notes on SCHAN that have been shifted
char SHIFTAMT = 0; // When SHIFT is enabled: Amount to shift NOTE-ONs on channel SCHAN by (-127 to 127)
byte SONG = 0; // Current song-slot whose data-files are being played
byte SWAPLAST = 255; // When SWAP is held, this tracks the previously-touched seq. Unlike RECORDSEQ, it is set to 255 when SWAP isn't held
word TEMPO = DEFAULT_TEMPO; // Current tempo (in microseconds-per-tick)

// Time-signature vars
byte RANGE = 8; // Number of GLOBAL CUE segments (this lets songs have structures with a number of bars that isn't a multiple of 8)
byte TIMESIG1 = 8; // Dividend of the current time-signature. Max range: 2 to 8.
byte TIMESIG2 = 12; // Divisor of the current time-signature. Max range: 7 to 12.
byte TIMESIG3 = 96; // Will always hold "TIMESIG1*TIMESIG2"

// Note-recording vars
byte HELDBUTTON = 0; // Held-note's corresponding button. |128 for MANUAL-DURATION.
word HELDPOS = 0; // Position in the RECORDSEQ where the held-note started playing and/or will be recorded, depending on context
byte TO_WRITE = 0; // Stores a "write" flag (1 = "write a command"), when a command is about to be recorded

// MIDI-IN vars
byte INBYTES[3] = {0, 0, 0}; // Buffer for incoming multi-byte MIDI commands
byte INCOUNT = 0; // Number of MIDI bytes received from the current incoming command
byte INTARGET = 0; // Number of expected incoming MIDI bytes
byte SYSIGNORE = 0; // Ignores all incoming MIDI messages when toggled (this is used to ignore SYSEX commands)

// File read/write data vars
// Array for reading all the bytes in a tick, on all tracks:
//// bytes 0 and 5: "MIDI-command type"
//// bytes 1 and 6: "first payload-byte" (pitch-byte, for NOTE-ONs)
//// bytes 2 and 7: "second payload-byte" (velocity-byte, for NOTE-ONs)
//// bytes 3 and 8: "note-duration" (empty for non-NOTE-ONs)
//// bytes 4 and 9: "chance of occurring"
byte BUFREAD[10];

// Array for writing the bytes of a single command on a single track, and/or tracking the properties of held or incoming MIDI-commands:
//// byte 0: "MIDI-command type"
//// byte 1: "first payload-byte" (pitch-byte, for NOTE-ONs)
//// byte 2: "second payload-byte" (velocity-byte, for NOTE-ONs)
//// byte 3: "note-duration" (empty for non-NOTE-ONs)
//// byte 4: "chance of occurring"
byte BUFWRITE[5] = {0, 0, 0, 0, 0};

// An array that is used by xorShift() to generate a cascade of semirandom 8-bit numbers.
byte RANDS[5] = {51, 181, 229, 21, 0};

// Cued-command flags, one per seq.
//// bit 0: TURN OFF
//// bit 1: TURN ON
//// bits 2-4: slice 4, 2, 1
//// bits 5-7: cue 4, 2, 1
byte CMD[48];

// Holds up to 10 active note-sustains
//// (format: SUST[n*3] = channel, pitch, duration)
byte SUST[30];
byte SUST_COUNT = 0; // Counts the current number of sustained notes in increments of 3 (one sustained note = 3 bytes)

// Holds the 48 seqs' internal tick-positions
word POS[48];

// Holds 48 seqs' sizes and activity-flags
//// bits 0-5: SEQ SIZE: 0 to 63 (+ 1 = seq size in measures)
//// bit 6: ACTIVE ON LOAD: this seq will start playing the moment the savefile is loaded.
//// bit 7: ON/OFF: this seq is either playing or not playing.
byte STATS[48];



// Typedef for a generic function that takes "col" and "row" arguments and returns nothing.
// In EDIT MODE, this is used with "col" and "row" values to select EDIT-actions based on which keychords are held.
typedef void (*CmdFunc) (byte col, byte row);

// Typedef for a generic function that takes no arguments and returns a byte.
// This is used to select which func should generate a value for the top row of the EDIT MODE GUI.
typedef byte (*GuiFunc) ();

// Typedef for a generic function that takes "ctrl", "col", and "row" arguments, and returns nothing.
// This is used to select PLAY MODE functions based on which keychords are active.
typedef void (*PlayFunc) (byte ctrl, byte col, byte row);



SdFat sd; // Initialize SdFat object, to control general SD-card operations
SdFile file; // Initialize an SdFile object, to control data read/write processes



// Testing code to see how much RAM is free at any given point.
// Can be used with two sendMaxCmd() calls to display current unused RAM on the LED screen, like so:
//// word n = word(countRam());
//// sendMaxCmd(7, byte(n >> 8));
//// sendMaxCmd(8, byte(n & 255));
// NOTE: Comment out this function when it is not in use!
/*
int countRam() {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}
*/



void setup() {
  
  // Ensure that the global arrays don't contain junk-data
  memset(BUFREAD, 0, 10);
  memset(CMD, 0, 48);
  memset(CUED_ONS, 0, MAX_ACTIVE_SEQS);
  memset(POS, 0, 96);
  memset(STATS, 0, 48);
  memset(SUST, 0, 30);
  
  // Set all the keypad's row-pins to INPUT_PULLUP mode, and all its column-pins to OUTPUT mode:
  // B bits 0, 1 = OUTPUT
  // C bits 0, 1, 2 = OUTPUT
  // C bits 3, 4, 5 = INPUT_PULLUP
  // D bits 2, 3, 4 = INPUT_PULLUP
  DDRB |= B00000011; // Set B(0, 1) as outputs
  DDRC = B00000111; // Set C(0, 1, 2) as outputs, and C(3, 4, 5) as inputs. C(6, 7) are unused
  DDRD &= B11100011; // Set D(2, 3, 4) as inputs
  PORTC |= B00111000; // Set pullup resistors for input-bits C(3, 4, 5)
  PORTD |= B00011100; // Set pullup resistors for input-bits D(2, 3, 4)
  
  maxInitialize(); // Initialize the MAX72** chip's LED system
  
  // Initialize the SD-card at full speed, or throw a visible error message (and show chip's fuse-bits) if no SD-card is inserted
  if (!sd.begin(10, SPI_FULL_SPEED)) {
    sendMaxCmd(1, boot_lock_fuse_bits_get(GET_LOW_FUSE_BITS));
    sendMaxCmd(2, boot_lock_fuse_bits_get(GET_HIGH_FUSE_BITS));
    sendMaxCmd(3, boot_lock_fuse_bits_get(GET_EXTENDED_FUSE_BITS));
    sendMaxCmd(4, B00000000);
    sendMaxCmd(5, B11011001);
    sendMaxCmd(6, B10010101);
    sendMaxCmd(7, B01010100);
    sendMaxCmd(8, B11011001);
    sd.initErrorHalt(); // Halt until the user restarts the device!
  }
  
  Serial.begin(31250); // Start serial comms at the MIDI baud rate
  
  midiPanic(); // Send NOTE-OFFs to every MIDI note on every MIDI channel
  
  createFiles(); // Check whether the savefiles exist, and if they don't, then create them
  
  loadPrefs(); // Load whatever prefs are in P.DAT, or create P.DAT if it doesn't exist yet
  
  loadSong(SONG); // Load whatever song-slot was in P.DAT, or the default song-slot if P.DAT didn't exist
  
  resetTiming(); // Reset all timing of all seqs and the global cue-point, and send SONG-POSITION POINTER if CLOCK SOURCE is internal
  
  ABSOLUTETIME = micros(); // Make sure ABSOLUTETIME matches the current time, so tempo doesn't have an initial jerk
  
}


void loop() {
  
  parseRawMidi(); // Parse incoming MIDI, if applicable
  
  updateTimer(); // Update the global timer
  
  updateGUI(); // Update the GUI, if applicable
  
}


