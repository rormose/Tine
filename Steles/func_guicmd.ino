


////////////////////////////////
// These functions all display different things in the top LED-row,
//   depending on which ctrl-command is currently held in EDIT MODE.
// References to them are kept in GUI_TOP_ROW,
//   and are called from the updateFirstRow() function.
// Also, some other functions call "xTR" specifically.
////////////////////////////////


// For ACTIVE ON LOAD, display a lit row if the current RECORDSEQ is ACTIVE ON LOAD
byte actOnLoadTR() { return (STATS[RECORDSEQ] & 64) ? 255 : 0; }

byte baseTR() { return BASENOTE; }

byte chanTR() { return CHAN; }

byte chanceTR() { return CHANCE; }

byte clockSourceTR() { return CLOCKSOURCE; }

byte durationTR() { return DURATION; }

byte gridConfigTR() { return GRIDCONFIG; }

byte humanizeTR() { return HUMANIZE; }

byte modifyTR() { return MODIFY; }

byte moveByTickTR() { return byte(POS[RECORDSEQ] >> 8); }

byte octaveTR() { return OCTAVE; }

// For OFFSET, translate its value into a byte, with a "below-zero" marker if OFFSET is negative.
byte offsetTR() { return ((OFFSET < 0) ? 128 : 0) | abs(OFFSET); }

byte qrstTR() { return QRESET; }

byte quantizeTR() { return QUANTIZE; }

byte repeatTR() { return REPEAT; }

// For SEQ SIZE, display the size of the current RECORDSEQ in whole-notes
byte sizeTR() { return (STATS[RECORDSEQ] & 63) + 1; }

// For TEMPO, send the top 8 bits of the TEMPO value; the rest will be displayed in the second LED-row
byte tempoTR() { return byte(TEMPO >> 8); }

// For TRACK, light up a large section of the row: left for TRACK 0, right for TRACK 1
byte trackTR() { return TRACK ? 15 : 240; }

byte tsDividendTR() { return TIMESIG1; }

byte tsDivisorTR() { return TIMESIG2; }

byte veloTR() { return VELO; }

// For commands without top-row LED requirements: display the current measure in the current sequence.
byte xTR() { return pgm_read_byte_near(SEQDISPLAY + (POS[RECORDSEQ] / TIMESIG3)); }


