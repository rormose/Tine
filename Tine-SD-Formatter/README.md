
# Tine-SD-Formatter
This is a variant of the "SdFormatter" sketch from the Sdfat library. It has been modified to run on Tine's buttons-and-LEDs interface, instead of a Serial Monitor window.

# Why, though?
Tine really, really needs an absolutely correctly-formatted industrial-grade SD-card, otherwise it can run into nasty lag when under heavy use. You must use this sketch to format any newly-bought industrial-grade SD-card that you're planning to use with Tine. (SD-cards that are sold alongside Tine units have already been formatted with this sketch, so you don't have to worry about them; this sketch is only necessary for formatting newly-bought SD-cards.)

# How to use
Refer to chapter 4 of Tine's manual, "Formatting New SD-Cards".
